export type NewspaperType = {
    place_of_publication: string;
    start_year: number;
    publisher: string;
    county: string[];
    frequency: string;
    url: string;
    id: string;
    subject: string[];
    city: string[];
    language: string[];
    title: string;
    holding_type: string[];
    end_year: number;
    alt_title: string[];
    note: string[];
    lccn: string;
    state: string[];
    place: string[];
    country: string;
    type: string;
    title_normal: string;
    oclc: string;
};

export type SearchNewspapersParams = {
    terms: string;
    page: number;
};

export type NewspapersSearchResult = {
    totalItems: number;
    startIndex: number;
    endIndex: number;
    itemsPerPage: number;
    items: NewspaperType[];
};