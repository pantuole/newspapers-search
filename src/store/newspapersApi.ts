import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { NewspapersSearchResult, SearchNewspapersParams } from '../types';

export const newspapersApi = createApi({
    reducerPath: 'newspapersApi',
    baseQuery: fetchBaseQuery({baseUrl: 'https://chroniclingamerica.loc.gov'}),
    endpoints: (builder) => ({
        getNewspapersByTerms: builder.query<NewspapersSearchResult, SearchNewspapersParams>({
            query: (params) => `/search/titles/results/?terms=${params.terms}&format=json&page=${params.page}`,
        }),
    }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const {useGetNewspapersByTermsQuery} = newspapersApi