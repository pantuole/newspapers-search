import { configureStore } from '@reduxjs/toolkit'
import { useDispatch, useSelector } from 'react-redux';
import { newspapersApi } from './newspapersApi.ts';
import { newspapersSlice } from './newspaperSlice.ts';

export const store = configureStore({
    reducer: {
        [newspapersApi.reducerPath]: newspapersApi.reducer,
        [newspapersSlice.reducerPath]: newspapersSlice.reducer,
    },
    // Adding the api middleware enables caching, invalidation, polling, and other useful features of `rtk-query`.
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(newspapersApi.middleware),
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export const useAppDispatch = useDispatch.withTypes<AppDispatch>();
export const useAppSelector = useSelector.withTypes<RootState>();