import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { NewspaperType } from "../types";

export type NewspapersState = {
    terms: string;
    page: number;
    selectedNewspaper: NewspaperType | null;
}

const initialState: NewspapersState = {
    selectedNewspaper: null,
    terms: "oakland",
    page: 1,
}

export const newspapersSlice = createSlice({
    name: 'newspapers',
    initialState,
    reducers: {
        selectNewsPaper: (state, action: PayloadAction<NewspaperType>) => {
            state.selectedNewspaper = action.payload;
        },
        setTerms: (state, action: PayloadAction<string>) => {
            state.selectedNewspaper = null;
            state.terms = action.payload;
        },
        prevPage: (state) => {
            if (state.page === 1) return;
            state.selectedNewspaper = null;
            state.page -= 1;
        },
        nextPage: (state) => {
            state.selectedNewspaper = null;
            state.page += 1;
        },
    },
})

export const {actions} = newspapersSlice