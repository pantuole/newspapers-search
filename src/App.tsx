// In this project containers refer to a smart function component that knows about store
import { NewspapersContainer } from './containers/NewspapersContainer.tsx';
import { SearchContainer } from './containers/SearchContainer.tsx';

import './App.css'

function App() {
    return (
        <>
            <SearchContainer/>
            <NewspapersContainer/>
        </>
    );
}

export { App };
