import { ChangeEvent, useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../store';
import { actions } from '../store/newspaperSlice.ts';
import './SearchContainer.css';

function SearchContainer() {
    const {terms, page} = useAppSelector((state) => state.newspapers);
    const [text, setText] = useState(terms);
    const dispatch = useAppDispatch();

    useEffect(() => {
        const timer = setTimeout(() => {
            dispatch(actions.setTerms(text));
        }, 500);
        return () => clearTimeout(timer);
    }, [text, dispatch]);

    const onInput = (e: ChangeEvent<HTMLInputElement>) => {
        setText(e.target.value);
    }

    const onNextPage = () => dispatch(actions.nextPage());

    const onPrevPage = () => dispatch(actions.prevPage());

    return (
        <div className={"search-input-container"}>
            <label htmlFor={"search-input"}>Search</label>
            <input
                id={"search-input"}
                type={"text"}
                value={text}
                onChange={onInput}
                placeholder={"Terms..."}
            />
            <button className={"paging-button"} onClick={onNextPage}>Next page</button>
            {page > 1 && (
                <button className={"paging-button"} onClick={onPrevPage}>Prev page</button>
            )}
        </div>
    );
}

export { SearchContainer };