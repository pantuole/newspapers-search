import { useGetNewspapersByTermsQuery } from '../store/newspapersApi.ts';
import { PublicationDetails } from '../components/PublicationDetails.tsx';
import { useAppDispatch, useAppSelector } from '../store';
import { RunningText } from '../components/RunningText.tsx';
import { actions } from '../store/newspaperSlice.ts';
import { Message } from '../components/Message.tsx';
import { NewspaperType } from '../types';

export function NewspapersContainer() {
    const {terms, selectedNewspaper, page} = useAppSelector((state) => state.newspapers);
    const {data, error, isFetching} = useGetNewspapersByTermsQuery({terms, page});
    const dispatch = useAppDispatch();

    const onPublicationSelect = (item: NewspaperType) => {
        dispatch(actions.selectNewsPaper(item));
    }

    if (isFetching) {
        return <Message message={"Loading newspapers"}/>;
    }

    if (error) {
        return <Message message={"Error occurred"}/>;
    }

    if (data && data.items.length === 0) {
        return <Message message={`No newspapers found for \`${terms}\``}/>
    }

    return (
        <div>
            {data && (
                <RunningText
                    data={data.items}
                    speed={80}
                    onItemClick={onPublicationSelect}
                    extractTextFn={(item) => item.title_normal}
                />
            )}
            {selectedNewspaper && (
                <PublicationDetails details={selectedNewspaper}/>
            )}
        </div>
    );
}