import { useMemo } from 'react';
import './RunningText.css';

type RunningTextProps<T> = {
    data: T[];
    speed?: number; // Speed in pixels per second
    runningWidth?: number;
    onItemClick: (item: T) => void;
    extractTextFn: (item: T) => string;
};

// In order to make text chunks selectable and don't think about id's
// we pass any array and extract text from every item
function RunningText<T>({
                            data,
                            speed = 20,
                            onItemClick,
                            extractTextFn,
                            runningWidth = document.body.clientWidth,
                        }: RunningTextProps<T>) {
    const duration = useMemo(() => {
        // Calculate the width of the text. Estimate 10px per character, adjust as needed
        const textWidth = data.reduce((acc, chunk) => acc + extractTextFn(chunk).length, 0) * 10;
        const animationDuration = (textWidth + runningWidth) / speed;
        return `${animationDuration}s`;
    }, [data, extractTextFn, speed, runningWidth]);

    return (
        <div className={"running-text-container"}>
            <div
                className={"running-text"}
                style={{animationDuration: duration}}>
                {data.map((item, index) => (
                    <span
                        key={index.toString()}
                        onClick={() => onItemClick(item)}
                    >
                        {extractTextFn(item)}
                    </span>
                ))}
            </div>
        </div>
    );
}

export { RunningText };