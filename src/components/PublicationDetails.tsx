import { NewspaperType } from "../types";
import "./PublicationDetails.css";

type Props = {
    details: NewspaperType;
}

function PublicationDetails({details}: Props) {
    const renderArray = (arr: string[]) => arr.join(', ');

    return (
        <div>
            <h2>{details.title}</h2>
            <div className={"data-container"}>
                <div className={"data-part"}>
                    <p><strong>Publication Place:</strong> {details.place_of_publication}</p>
                    <p><strong>Start Year:</strong> {details.start_year}</p>
                    <p><strong>Publisher:</strong> {details.publisher}</p>
                    <p><strong>Counties:</strong> {renderArray(details.county)}</p>
                    <p><strong>Frequency:</strong> {details.frequency}</p>
                    <p><strong>URL:</strong> <a href={details.url}>Link</a></p>
                    <p><strong>Subjects:</strong> {renderArray(details.subject)}</p>
                    <p><strong>Cities:</strong> {renderArray(details.city)}</p>
                    <p><strong>Languages:</strong> {renderArray(details.language)}</p>
                </div>
                <div className={"data-part"}>
                    <p><strong>Holding Types:</strong> {renderArray(details.holding_type)}</p>
                    <p><strong>End Year:</strong> {details.end_year}</p>
                    <p><strong>Alternative Titles:</strong> {renderArray(details.alt_title)}</p>
                    <p><strong>Notes:</strong> {renderArray(details.note)}</p>
                    <p><strong>States:</strong> {renderArray(details.state)}</p>
                    <p><strong>Places:</strong> {renderArray(details.place)}</p>
                    <p><strong>Country:</strong> {details.country}</p>
                    <p><strong>Type:</strong> {details.type}</p>
                    <p><strong>Normalized Title:</strong> {details.title_normal}</p>
                </div>
            </div>
        </div>
    );
}

export { PublicationDetails };