import reactLogo from '../assets/react.svg';

type Props = { message: string };

function Message({message}: Props) {
    return (
        <div>
            <img src={reactLogo} className="logo react" alt="React logo"/>
            <p>{message}</p>
        </div>
    );
}

export { Message };